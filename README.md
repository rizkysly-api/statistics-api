# Statistics API 
Collect and store basic statistics. It will store the number of visitors, sessions and screen views per day. Details about visited screens and performed actions. Metrics for make/model, language/country, application/version and os/version.

## Installation
`git clone https://gitlab.com/rizkysly-api/statistics-api.git`  
`git submodule update --init --recursive`

Import the `database.sql` file into your mysql database.

Rename `configuration_example.php` to configuration.php and change the files constants.  

Setup an url-rewrite rule to send all traffic to `index.php`.  

Add the following cron tasks by running `crontab -e`
```
0 0 * * * php -f index.php clean
*/30 * * * * php -f index.php process
```

## Requests
Please see https://gitlab.com/rizkysly-api/api-generics.git for request specifications. The post values here are on top of the once described in api-generics.

**/add**  
Save the statistics of the device.
>>>
data={"paths": [{"timestamp": 0, "path": "", "info": ""}],"actions": [{"timestamp": 0, "action": "", "info": ""}], "metrics": {"type": "", "value1": "", "value2": ""}}
>>>
The allowed metric types are defined in the configuration php.