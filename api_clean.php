<?php

class api_clean
{

    // After a certain period we simply ignore statistics, so clean the database 
    public static function go()
    {
        // Clean up tmp tables till the oldes date we want to remember
        $stmt = api_database::query('DELETE FROM statistics_tmp_dailyvisitors WHERE date < DATE(NOW() - INTERVAL ' . IGNORE_MYSQL_INTERVAL . ')');
        $stmt = api_database::query('DELETE FROM statistics_tmp_sessions WHERE last_datetime < DATE(NOW() - INTERVAL ' . IGNORE_MYSQL_INTERVAL . ')');
    }
}