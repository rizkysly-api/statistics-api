<?php

// Lets process the information in the database

// The data we expect to receive. The first level of items should be available at all times.
/*
$data = [
    'paths' => [['timestamp' => time(), 'path' => 'path1', 'info' => '']],
    'actions' => [['timestamp' => time(), 'action' => 'path1', 'info' => '']],
    'metrics => ['type' => '', 'value1' => '', 'value2' => '']
];
 */

class api_process
{
    // Process the entries in the received table, it should not matter how many times this is called
    public static function received() : void
    {
        // Create unique id so we know what entries we are handling now.
        $processing_uuid = api_uuidv4::generate(); 

        // Mark entire as processing, from old to new, limiting the number by...
        $stmt = api_database::prepare('UPDATE statistics_tmp_received SET processing_uuid = ? WHERE processing_uuid IS NULL ORDER BY id LIMIT ' . PROCESSING_ENTRY_LIMIT);
        $stmt->bind_param('s', $processing_uuid);
        $stmt->execute();
        api_database::check();
        $stmt->close();

        // Get all entries we need to process, and merge duplicate device entries
        $entries = self::get($processing_uuid);

        // Make sure all devices are sorted and remove any doubles paths or actions
        $entries = self::sort($entries);

        // First handle the visitor data, a visitor is unique during the day, so check for the date in tmp_dailyvisitors
        self::dailyVisitors($entries);

        // Now handle the visits stuff, a new visit starts if the last_datetime is older then SESSION_MINUTES
        self::sessions($entries);

        // And record the individual paths and actions (and add up screens)
        self::pathsActions($entries);

        // Remove all entries we have just processed
        $stmt = api_database::prepare('DELETE FROM statistics_tmp_received WHERE processing_uuid = ?');
        $stmt->bind_param('s', $processing_uuid);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }

    // Get all entries we need to process, and merge duplicate device entries
    private static function get($processing_uuid) : array
    {
        $entries = [];
        $stmt = api_database::prepare('SELECT app_uuid, device_uuid, data FROM statistics_tmp_received WHERE processing_uuid = ? ORDER BY id');
        $stmt->bind_param('s', $processing_uuid);
        $stmt->execute();
        $stmt->bind_result($app_uuid, $device_uuid, $json);
        while ($stmt->fetch()) {
            // Valid JSON?
            $data = json_decode($json, true);
            if (json_last_error() != JSON_ERROR_NONE) {
                continue;
            }

            // Are all keys present? 
            if (!isset($data['paths']) || !isset($data['actions']) || !isset($data['metrics'])) {
                continue;
            }

            // Lets see if those three are array's
            if (!is_array($data['paths']) || !is_array($data['actions']) || !is_array($data['metrics'])) {
                continue;
            }

            // Lets see if those have the correct field
            foreach ($data['paths'] as $path) {
                if (!isset($path['timestamp']) || !isset($path['path']) || !isset($path['info'])) {
                    continue 2;
                }
            }
            foreach ($data['actions'] as $action) {
                if (!isset($action['timestamp']) || !isset($action['action']) || !isset($action['info'])) {
                    continue 2;
                }
            }
            foreach ($data['metrics'] as $metric) {
                if (!isset($metric['type']) || !isset($metric['value1']) || !isset($metric['value2'])) {
                    continue 2;
                }
            }

            // One of these should be filled, it contains the timestamp
            if (count($data['paths']) == 0 && count($data['actions']) == 0) {
                continue;
            }

            // Create array's if the device does not exist yet
            if (!isset($entries[$device_uuid])) {
                $entries[$device_uuid] = ['paths' => [], 'actions' => [], 'metrics' => []];
            }

            // Append paths and actions if we encounter the same device multiple times
            $entries[$device_uuid]['paths'] = array_merge($entries[$device_uuid]['paths'], $data['paths']);
            $entries[$device_uuid]['actions'] = array_merge($entries[$device_uuid]['actions'], $data['actions']);

            // We only keep one copy of this one, the last one
            $entries[$device_uuid]['app_uuid'] = $app_uuid;
            $entries[$device_uuid]['metrics'] = $data['metrics'];
        }
        $stmt->close();

        return $entries;
    }

    private static function sort(array $entries) : array
    {
        foreach ($entries as $device => $data) {
            $entries[$device]['paths'] = array_unique($data['paths'], SORT_REGULAR);
            usort($entries[$device]['paths'], function ($a, $b) {
                return $a['timestamp'] <=> $b['timestamp'];
            });

            $entries[$device]['actions'] = array_unique($data['actions'], SORT_REGULAR);
            usort($entries[$device]['actions'], function ($a, $b) {
                return $a['timestamp'] <=> $b['timestamp'];
            });
        }
        return $entries;
    }

    // Count the daily visitors and store other metric data
    private static function dailyVisitors(array $entries) : void
    {
        foreach ($entries as $device => $data) {
            // Get all dates we have signs of live
            $dates = [];
            foreach ($data['paths'] as $path) {
                $dates[] = gmdate('Y-m-d', $path['timestamp']);
            }
            foreach ($data['actions'] as $action) {
                $dates[] = gmdate('Y-m-d', $action['timestamp']);
            }

            // We only want unique dates, and sort them to reset the array key's
            if (count($dates) > 1) {
                $dates = array_unique($dates);
                sort($dates);
            }

            // Add the device dates to the database
            foreach ($dates as $date) {

                // Ignore entries that are to old
                if ($date < gmdate('Y-m-d', strtotime(strtolower('-' . IGNORE_MYSQL_INTERVAL)))) {
                    continue;
                }

                // If not in error, it is a new daily visitor
                $error = false;
                $stmt = api_database::prepare('INSERT INTO statistics_tmp_dailyvisitors (device_uuid, date) VALUES (?, ?)');
                $stmt->bind_param('ss', $device, $date);
                $stmt->execute();
                $error = $stmt->error;
                $stmt->close();

                // Not new? ...ignore... (When not new, duplicate key error occures, and the user is allready counted for that day)
                if (strlen($error) > 0) {
                    continue;
                }

                // App
                $app = APP_KEYS[$data['app_uuid']];

                // Count visitor for the day
                $stmt = api_database::prepare('INSERT INTO statistics_usage (app, date, visitors) VALUES (?, ?, 1) ON DUPLICATE KEY UPDATE visitors = visitors + 1');
                $stmt->bind_param('ss', $app['name'], $date);
                $stmt->execute();
                api_database::check();
                $stmt->close();

                // Other interesting data
                foreach ($data['metrics'] as $metric) {
                    if (!in_array($metric['type'], $app['metrics'])) {
                        continue;
                    }
                    $metric['value1'] = substr($metric['value1'], 0, 255);
                    $metric['value2'] = substr($metric['value2'], 0, 255);

                    $stmt = api_database::prepare('INSERT INTO statistics_data (app, date, type, value1, value2) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE counter = counter + 1');
                    $stmt->bind_param('sssss', $app['name'], $date, $metric['type'], $metric['value1'], $metric['value2']);
                    $stmt->execute();
                    api_database::check();
                    $stmt->close();
                }
            }
        }
    }

    private static function sessions(array $entries) : void
    {
        foreach ($entries as $device => $data) {

            // Get last session date from database
            $stmt = api_database::prepare('SELECT UNIX_TIMESTAMP(last_datetime) FROM statistics_tmp_sessions WHERE device_uuid = ?');
            $stmt->bind_param('s', $device);
            $stmt->execute();
            $stmt->bind_result($lastsession);
            $stmt->fetch();
            $stmt->close();

            // Get all datetimes we have signs of live
            $timestamps = [];
            foreach ($data['paths'] as $path) {
                $timestamps[] = $path['timestamp'];
            }
            foreach ($data['actions'] as $action) {
                $timestamps[] = $action['timestamp'];
            }

            // We only want unique dates, and sort them to reset the array key's
            $timestamps = array_unique($timestamps);
            sort($timestamps);

            // Determin if a new session is detected
            $sessions = [];
            foreach ($timestamps as $timestamp) {
                // Ignore entries that are to old
                if ($timestamp < strtotime(strtolower('-' . IGNORE_MYSQL_INTERVAL))) {
                    continue;
                }

                $date = gmdate('Y-m-d', $timestamp);
                if (!isset($sessions[$date])) {
                    $sessions[$date] = 0;
                }

                // New session if times between items is largder then...
                if ($timestamp - $lastsession > SESSION_MINUTES * 60) {
                    $sessions[$date]++;
                    $lastsession = $timestamp;
                }
            }

            // App
            $app = APP_KEYS[$data['app_uuid']];

            // Store sessions
            foreach ($sessions as $date => $counter) {
                if ($counter > 0) {
                    //Usage already exists, it is created when looking for daily visitors
                    $stmt = api_database::prepare('UPDATE statistics_usage SET sessions = sessions + ? WHERE app = ? AND date = ?');
                    $stmt->bind_param('iss', $counter, $app['name'], $date);
                    $stmt->execute();
                    api_database::check();
                    $stmt->close();
                }
            }

            // Store last known session time
            $stmt = api_database::prepare('INSERT INTO statistics_tmp_sessions (device_uuid, last_datetime) VALUES (?, FROM_UNIXTIME(?)) ON DUPLICATE KEY UPDATE last_datetime = FROM_UNIXTIME(?)');
            $stmt->bind_param('sii', $device, $lastsession, $lastsession);
            $stmt->execute();
            api_database::check();
            $stmt->close();
        }
    }

    // Store the individual paths (screens or views) and actions
    private static function pathsActions(array $entries) : void
    {
        foreach ($entries as $device => $data) {
            $items = [];

            // App
            $app = APP_KEYS[$data['app_uuid']];

            foreach ($data['paths'] as $path) {
                // Ignore entries that are to old
                if ($path['timestamp'] < strtotime(strtolower('-' . IGNORE_MYSQL_INTERVAL))) {
                    continue;
                }

                // It could hold multiple dates
                $date = gmdate('Y-m-d', $path['timestamp']);
                if (!isset($items[$date])) {
                    $items[$date] = [];
                }

                // Create a place to count
                $key = 'path|' . $path['path'] . '|' . $path['info'];
                if (!isset($items[$date][$key])) {
                    $items[$date][$key] = [
                        'type' => 'path',
                        'value1' => $path['path'],
                        'value2' => $path['info'],
                        'counter' => 0
                    ];
                }

                // Count up all items
                $items[$date][$key]['counter']++;
            }

            foreach ($data['actions'] as $action) {
                // Ignore entries that are to old
                if ($action['timestamp'] < strtotime(strtolower('-' . IGNORE_MYSQL_INTERVAL))) {
                    continue;
                }

                // It could hold multiple dates
                $date = gmdate('Y-m-d', $action['timestamp']);
                if (!isset($items[$date])) {
                    $items[$date] = [];
                }

                // Create a place to count
                $key = 'action|' . $action['action'] . '|' . $action['info'];
                if (!isset($items[$date][$key])) {
                    $items[$date][$key] = [
                        'type' => 'action',
                        'value1' => substr($action['action'], 0, 255),
                        'value2' => substr($action['info'], 0, 255),
                        'counter' => 0
                    ];
                }

                // Count up all items
                $items[$date][$key]['counter']++;
            }

            foreach ($items as $date => $keys) {
                $screens = 0;

                foreach ($keys as $key => $item) {
                    $stmt = api_database::prepare('INSERT INTO statistics_data (app, date, type, value1, value2, counter) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE counter = counter + ?');
                    $stmt->bind_param('sssssii', $app['name'], $date, $item['type'], $item['value1'], $item['value2'], $item['counter'], $item['counter']);
                    $stmt->execute();
                    api_database::check();
                    $stmt->close();

                    if ($item['type'] == 'path') {
                        $screens += $item['counter'];
                    }
                }

                if ($screens > 0) {
                    // Row should already exist...
                    $stmt = api_database::prepare('UPDATE statistics_usage SET screens = screens + ? WHERE app = ? AND date = ?');
                    $stmt->bind_param('iss', $screens, $app['name'], $date);
                    $stmt->execute();
                    api_database::check();
                    $stmt->close();
                }
            }
        }
    }
}