<?php

// Just store the information, we'll process this later

class api_received
{

    // Store the data in the databae, check as little as possible, we'll do that later
    public static function add() : void
    {
        // Without data we are lost...
        if (!isset($_POST['data'])) {
            api_security::generateError('400 Bad Request (data)');
        }

        // We do expect valid json...
        json_decode($_POST['data'], false, 4);
        if (json_last_error() != JSON_ERROR_NONE) {
            api_security::generateError('400 Bad Request (data depth)');
        }

        // Now store it to database so we can process this later on
        $stmt = api_database::prepare('INSERT INTO statistics_tmp_received (app_uuid, device_uuid, data) VALUES (?, ?, ?)');
        $stmt->bind_param('sss', $_SERVER['HTTP_X_APPLICATION_KEY'], $_POST['device'], $_POST['data']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }
}