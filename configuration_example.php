<?php

// List all applications keys with the following information:
// - App secrets to check the request signatures
// - The app name to list in the database
define('APP_KEYS', [
    'APP_KEY' => [
        'secret' => 'APP_SECRET',
        'name' => 'app name',
        'metrics' => ['application', 'device', 'operatingsystem', 'locale']
    ]
]);

// Talking to a database require database credentials
define('DB_HOST', 'localhost');
define('DB_DATABASE', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');

// Some throtteling stuff
define('FAILED_ATTEMPTS_NUMBER', 10); // How many failed attempts can an ip make
define('FAILED_ATTEMPTS_MINUTES', 10); // In how much time
define('TIMESTAMP_AGE_SECONDS', 300); // What can the timestamp difference be

// Ignore statistics before
define('IGNORE_MYSQL_INTERVAL', '1 WEEK');

// What do we consider a session
define('SESSION_MYSQL_INTERVAL', '30 MINUTE');

// Lets limit the number of entries we process at a time, we can increment it if this number is to low
define('PROCESSING_ENTRY_LIMIT', 100);
