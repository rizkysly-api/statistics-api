-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 30 dec 2018 om 13:14
-- Serverversie: 5.7.24-0ubuntu0.18.04.1
-- PHP-versie: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_rizkysly`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `statistics_data`
--

CREATE TABLE `statistics_data` (
  `app` varchar(30) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `type` varchar(30) NOT NULL DEFAULT '',
  `value1` varchar(255) NOT NULL DEFAULT '',
  `value2` varchar(255) NOT NULL DEFAULT '',
  `counter` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `statistics_tmp_dailyvisitors`
--

CREATE TABLE `statistics_tmp_dailyvisitors` (
  `device_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `statistics_tmp_received`
--

CREATE TABLE `statistics_tmp_received` (
  `id` int(11) NOT NULL,
  `app_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `device_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `data` text,
  `processing_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `statistics_tmp_sessions`
--

CREATE TABLE `statistics_tmp_sessions` (
  `device_uuid` char(36) NOT NULL,
  `last_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `statistics_usage`
--

CREATE TABLE `statistics_usage` (
  `app` varchar(30) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `visitors` int(11) NOT NULL DEFAULT '0',
  `sessions` int(11) NOT NULL DEFAULT '0',
  `screens` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `statistics_data`
--
ALTER TABLE `statistics_data`
  ADD PRIMARY KEY (`app`,`date`,`type`(1),`value1`,`value2`);

--
-- Indexen voor tabel `statistics_tmp_dailyvisitors`
--
ALTER TABLE `statistics_tmp_dailyvisitors`
  ADD PRIMARY KEY (`device_uuid`,`date`);

--
-- Indexen voor tabel `statistics_tmp_received`
--
ALTER TABLE `statistics_tmp_received`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `statistics_tmp_sessions`
--
ALTER TABLE `statistics_tmp_sessions`
  ADD PRIMARY KEY (`device_uuid`);

--
-- Indexen voor tabel `statistics_usage`
--
ALTER TABLE `statistics_usage`
  ADD PRIMARY KEY (`app`,`date`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `statistics_tmp_received`
--
ALTER TABLE `statistics_tmp_received`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
