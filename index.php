<?php

// Quick and dirty, and it works...
include(__DIR__ . '/configuration.php');
include(__DIR__ . '/api-generics/api_database.php');
include(__DIR__ . '/api-generics/api_request.php');
include(__DIR__ . '/api-generics/api_security.php');
include(__DIR__ . '/api-generics/api_uuidv4.php');
include(__DIR__ . '/api_received.php');
include(__DIR__ . '/api_clean.php');
include(__DIR__ . '/api_process.php');

// Handle cli requests (cron jobs)
if (php_sapi_name() == 'cli') {
    if (!isset($argv[1])) {
        exit(0);
    }

    switch ($argv[1]) {
        case 'clean':
            api_clean::go();
            break;
        case 'process':
            api_process::received();
            break;
    }

    exit(0);
}


// Checks if the request is valid, signed and so on
api_security::checkRequest();

// Routing; Just make sure all traffic is rewrited to this file
$action = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_BASENAME);
switch ($action) {
    case 'add':
        api_received::add();
        break;
    default:
        self::generateError('404 File not found');
}

// If everything is fine, we do not return any data, just a 200 status code, exept when it is expected
